// --------------------------------------------------------------
   // Developed By    : Tanvirul Haque
   // Version         : 1.0
// --------------------------------------------------------------

(function ($) {
    "use strict";
	
	jQuery(document).ready(function($){
		
		// 01. START PRELOADER
		$(window).load(function() {
		// Animate loader off screen
			$(".preloader").fadeOut("slow");
		});
		// 01. END PRELOADER
		
		// 02. START MENU STICKY JS
		$(".header-area").sticky({
            topSpacing: 0,
        });
		// 02. END MENU STICKY JS
		
		// 03. START SMOTH SCROOL JS
		$('.mainmenu li a.smoth-scroll').click(function() {
			if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
				var target = $(this.hash);
				target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
				if (target.length) {
					$('html, body').animate({
						scrollTop: target.offset().top - 60
					}, 1000);
					return false;
				}
			}
		});
		// 03. END SMOTH SCROOL JS
		
		// 05. START TOGGLE DROPDOWN JS
		$(document).on('click','.navbar-collapse.in',function(e) {
		if( $(e.target).is('a') && $(e.target).attr('class') != 'dropdown-toggle' ) {
				$(this).collapse('hide');
			}
		});
		// 05. END TOGGLE DROPDOWN JS

		$("#logo-carousel").owlCarousel({
			items: 5,
			margin: 30,
			loop: true,
			autoplay: true,
			autoplayTimeout: 5000,
			nav: false,
			dots: false, 
			responsive: {
				0:{
					items: 1,
				},
				600:{
					items: 3,
				},
				1000:{
					items: 5,
				},
			}
		})

		$("#testimonial-carousel").owlCarousel({
			items: 1,
			loop: true,
			autoplay: true,
			autoplayTimeout: 5000,
			nav: false,
			dots: false
		})

	});
	
})(jQuery);