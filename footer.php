<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Incorta
 */
$footer_social_options = cs_get_option('footer_social_options');
$footer_contact_options = cs_get_option('footer_contact_options');
$footer_copyright_text = cs_get_option('footer_copyright_text');
?>
	<footer  id="colophon" class="footer-area section-padding site-footer">
		<div class="container">
			<div class="row">
				<div class="footer-top clearfix">
					<div class="col-md-3 col-sm-4">
						<div class="footer-logo">
							<?php the_custom_logo(); ?>
						</div>
					</div>
					<div class="col-md-6 col-sm-8">
						<div class="footer-menu text-center">
							<?php incorta_footer_menu(); ?>
						</div>
					</div>
					<div class="col-md-3 col-sm-12">
						<div class="footer-social-icon text-right">
							<ul class="list-inline">
								<?php 
								if ( !empty( $footer_social_options ) ) {
									foreach ( $footer_social_options as $footer_social_option ) {
									?>
										<li><a href="<?php echo esc_url( $footer_social_option['footer_social_link'] ) ?>" target="_blank"><i class="<?php echo esc_attr( $footer_social_option['footer_social_icon'] ) ?>"></i></a></li>
									<?php
									}
								}
								?>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="footer-bottom clearfix">
					<div class="col-md-6 col-sm-12">
						<div class="copyrights">
							<p>
								<?php 
									if ( !empty( $footer_copyright_text ) ) {
										echo incorta_wp_kses( $footer_copyright_text );
									} else {
										esc_html_e( '&copy; Copyright 2018 incorta - All Rights Reserved.', 'incorta' );
									}
								?>
							</p>
						</div>
					</div>
					<div class="col-md-6 col-sm-12">
						<div class="contact-info text-right">
							<ul class="list-inline">
								<?php 
									if ( !empty( $footer_contact_options ) ) {
										foreach ( $footer_contact_options as $footer_contact_option ) {
										?>
											<li><i class="<?php echo esc_attr( $footer_contact_option['footer_contact_info_icon'] ) ?>"></i><?php echo esc_html( $footer_contact_option['footer_contact_info_text'] ) ?></li>
										<?php
										}
									}
								 ?>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</footer>

<?php wp_footer(); ?>

</body>
</html>
