<?php
/**
 * Incorta functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Incorta
 */

if ( ! function_exists( 'incorta_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function incorta_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on Incorta, use a find and replace
		 * to change 'incorta' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'incorta', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );
		add_image_size( 'incorta-blog-thumb', 750, 450, true );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'top_menu' => esc_html__( 'Top Menu', 'incorta' ),
			'footer_menu' => esc_html__( 'Footer Menu', 'incorta' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'incorta_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );
		
		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );

		// Add theme editor style.
		add_editor_style( 'assets/css/editor-style.css' );
	}
endif;
add_action( 'after_setup_theme', 'incorta_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function incorta_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'incorta_content_width', 640 );
}
add_action( 'after_setup_theme', 'incorta_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function incorta_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'incorta' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'incorta' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'incorta_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function incorta_scripts() {
	wp_enqueue_style( 'custom-google-fonts', 'https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700', false );
	
	wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/assets/bootstrap/css/bootstrap.min.css', array(), '4.7' );
	wp_enqueue_style( 'owlcarousel', get_template_directory_uri() . '/assets/owlcarousel/css/owl.carousel.css', array(), '2' );
	wp_enqueue_style( 'font-awesome', get_template_directory_uri() . '/assets/fonts/font-awesome.min.css', array(), '3.3.7' );
	wp_enqueue_style( 'incorta-deafult', get_template_directory_uri() . '/assets/css/deafult.css', array(), '1.0' );
	wp_enqueue_style( 'incorta-style', get_stylesheet_uri() );
	wp_enqueue_style( 'incorta-responsive', get_template_directory_uri() . '/assets/css/responsive.css', array(), '1.0' );


	wp_enqueue_script( 'bootstrap', get_template_directory_uri() . '/assets/bootstrap/js/bootstrap.min.js', array('jquery'), '20151215', true );
	wp_enqueue_script( 'owlcarousel', get_template_directory_uri() . '/assets/owlcarousel/js/owl.carousel.min.js', array('jquery'), '20151215', true );
	wp_enqueue_script( 'sticky', get_template_directory_uri() . '/assets/js/jquery.sticky.js', array('jquery'), '20151215', true );
	wp_enqueue_script( 'incorta-scripts', get_template_directory_uri() . '/assets/js/scripts.js', array('jquery'), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'incorta_scripts' );

// Add Header Menu
function incorta_main_menu() {
	if ( has_nav_menu( 'top_menu' ) ) {
		wp_nav_menu(
			array(
				'theme_location'    => 'top_menu',
				'container'         => false,
				'fallback_cb'		=> false,
				'menu_class'        => 'nav navbar-nav navbar-right',
			)
		);
	} else {
		echo '<ul class="'.esc_attr( 'nav navbar-nav navbar-right' ).'"><li><a href="'.admin_url( 'nav-menus.php' ).'">'.esc_html__( 'Add A Menu', 'incorta' ).'</a></li></ul>';
	}
}

// Add Footer Menu
function incorta_footer_menu() {
	if ( has_nav_menu( 'footer_menu' ) ) {
		wp_nav_menu(
			array(
				'theme_location'    => 'footer_menu',
				'container'         => false,
				'fallback_cb'		=> false,
				'menu_class'        => 'list-inline',
			)
		);
	} else {
		echo '<ul class="'.esc_attr( 'list-inline' ).'"><li><a href="'.admin_url( 'nav-menus.php' ).'">'.esc_html__( 'Add Footer Menu', 'incorta' ).'</a></li></ul>';
	}
}

// Supported wp_kses
function incorta_wp_kses($val){
	return wp_kses($val, array(
	
	'p' => array(),
	'span' => array(),
	'div' => array(),
	'strong' => array(),
	'b' => array(),
	'br' => array(),
	'h1' => array(),
	'h2' => array(),
	'h3' => array(),
	'h4' => array(),
	'h5' => array(),
	'h6' => array(),
	'ul' => array(),
	'ol' => array(),
	'li' => array(),
	'a'=> array('href' => array(),'target' => array()),
	'iframe'=> array('src' => array(),'height' => array(),'width' => array()),
	'img'=> array('src' => array(),'alt' => array()),
	
	), '');
}

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Theme Options and Metabox Framework
 */
require get_template_directory() . '/inc/cs-framework/cs-framework.php';
require get_template_directory() . '/inc/metabox-and-options.php';
