<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Incorta
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">

	<?php 
	$enable_preloader = cs_get_option('enable_preloader');
	wp_head(); ?>
</head>

<body <?php body_class(); ?>>

	<!-- START PRELOADER -->
	<?php if ( $enable_preloader == true ) : ?>
		<div class="preloader">
	        <div class="spinner">
				<div class="rect1"></div>
				<div class="rect2"></div>
				<div class="rect3"></div>
				<div class="rect4"></div>
				<div class="rect5"></div> 
	        </div>
	    </div>
	<?php endif; ?>
	<!-- / END PRELOADER -->

	<header class="header-area">
		<div class="container">
			<div class="navbar navbar-default main-menu">

				<div class="navbar-header">
					<button data-target=".navbar-collapse" data-toggle="collapse" class="navbar-toggle" type="button">
						<span class="sr-only"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="index.html">
						<?php the_custom_logo(); ?>
					</a>
				</div>

				<div class="navbar-collapse collapse">
					<nav>
						<?php incorta_main_menu(); ?>
					</nav>
				</div>
			</div>
		</div>
	</header>