<?php if ( ! defined( 'ABSPATH' ) ) { die; } // Cannot access pages directly.

// remove default shortcode options
function incorta_shortcode_options( $options ) {
	$options      = array();
}
add_filter( 'cs_shortcode_options', 'incorta_shortcode_options' );

// remove default customizer 
function incorta_customizer( $options ) {
	$options      = array();
}
add_filter( 'cs_customize_options', 'incorta_customizer' );


function incorta_metabox_options( $options ) {

	$options      = array(); // remove old options
	/*-------------------
	Page Meta Options
	---------------------*/
	$options[]    = array(
		'id'        => 'incorta_page_options',
		'title'     => esc_html__('Page Options', 'incorta'),
		'post_type' => 'page',
		'context'   => 'normal',
		'priority'  => 'high',
		'sections'  => array(
			array(
				'name' => 'incorta_page_options_meta',
				'icon'  => 'fa fa-cog',
				'fields' => array(
					array(
						'id'    => 'enable_title',
						'type'  => 'switcher',
						'title' => esc_html__('Enable Page Title?', 'incorta'),
						'default' => true,
						'desc' => esc_html__('If you want to enable Page title, please select On', 'incorta'),
					),
					array(
						'id'    => 'custome_title',
						'type'  => 'text',
						'title' => esc_html__('Custome Page Title?', 'incorta'),
						'dependency'   => array( 'enable_title', '==', 'true' ),
						'desc' => esc_html__('If you want to custome Page title, please type here.', 'incorta'),
					),
				),
			)
		),
	);

	return $options;
}
add_filter( 'cs_metabox_options', 'incorta_metabox_options' );


function incorta_theme_option_settings ( $settings ) {

	$settings = array();

	$settings	= array(
		'menu_title'      => esc_html__('Theme Options', 'incorta'),
		'menu_type'       => 'theme',
		'menu_slug'       => 'incorta-theme-options',
		'ajax_save'       => true,
		'show_reset_all'  => true,
		'framework_title' => esc_html__('Incorta. - Theme Options', 'incorta'),
	);

	return $settings;
}
add_filter('cs_framework_settings', 'incorta_theme_option_settings');


function incorta_theme_options($options){
       
    $options = array(); // remove old opotions

	$options[]    = array(
		'name'      => 'incorta_general_setting',
		'title'     => esc_html__('General Setting', 'incorta'),
		'icon'      => 'fa fa-cogs',
		'fields'    => array(
			array(
              'id'      => 'enable_preloader',
              'type'    => 'switcher',
              'title'   => esc_html__('Enable Preloader', 'incorta'),
              'default' => true,
              'label' => esc_html__('Select ON for show preloader.', 'incorta'),
            ),
		)
	);

	$options[]    = array(
		'name'      => 'incorta_blog_setting',
		'title'     => esc_html__('Blog Setting', 'incorta'),
		'icon'      => 'fa fa-newspaper-o',
		'fields'    => array(
			array(
				'id'        => 'post_by',
				'type'      => 'switcher',
				'title'     => esc_html__('Display Post By?', 'incorta'),
				'default'   => true,
				'label' => esc_html__('Select ON for show Post By.', 'incorta'),
			),
			array(
				'id'        => 'post_date',
				'type'      => 'switcher',
				'title'     => esc_html__('Display Post Date?', 'incorta'),
				'default'   => true,
				'label' => esc_html__('Select ON for show post date.', 'incorta'),
			),
			array(
				'id'        => 'post_comment',
				'type'      => 'switcher',
				'title'     => esc_html__('Display Comment Counts?', 'incorta'),
				'default'   => true,
				'label' => esc_html__('Select ON for show comment counts.', 'incorta'),
			),
			array(
				'id'        => 'post_category',
				'type'      => 'switcher',
				'title'     => esc_html__('Display Post Categories?', 'incorta'),
				'default'   => true,
				'label' => esc_html__('Select ON for show post categories.', 'incorta'),
			),
			array(
				'id'        => 'post_tag',
				'type'      => 'switcher',
				'title'     => esc_html__('Display Post Tags?', 'incorta'),
				'default'   => true,
				'label' => esc_html__('Select ON for show post tags.', 'incorta'),
			),
			array(
				'id'        => 'post_nav',
				'type'      => 'switcher',
				'title'     => esc_html__('Display Post Navigation?', 'incorta'),
				'default'   => true,
				'label' => esc_html__('Select ON for show post navigation.', 'incorta'),
			),
		)
	);

	$options[]    = array(
		'name'      => 'incorta_footer_setting',
		'title'     => esc_html__('Footer Setting', 'incorta'),
		'icon'      => 'fa fa-pencil-square-o',
		'fields'    => array(
			array(
				'id'              => 'footer_social_options',
				'type'            => 'group',
				'title'           => esc_html__('Social Icons', 'incorta'),
				'button_title'    => esc_html__('Add New Icon', 'incorta'),
				'accordion_title' => esc_html__('Add New Icon', 'incorta'),
				'fields'          => array(
					array(
						'id'    => 'footer_social_icon',
						'type'  => 'icon',
						'title' => esc_html__('Choose Icon', 'incorta'),
						'desc'	=> esc_html__('Add a social icon here.', 'incorta'),
					),
					array(
						'id'    => 'footer_social_link',
						'type'  => 'text',
						'title' => esc_html__('Type URL', 'incorta'),
						'desc'	=> esc_html__('Enter social link here.', 'incorta'),
					),
				),
			),

			array(
				'id'              => 'footer_contact_options',
				'type'            => 'group',
				'title'           => esc_html__('Footer Contact Info', 'incorta'),
				'button_title'    => esc_html__('Add New Info', 'incorta'),
				'accordion_title' => esc_html__('Add New Info', 'incorta'),
				'fields'          => array(
					array(
						'id'    => 'footer_contact_info_icon',
						'type'  => 'icon',
						'title' => esc_html__('Choose Icon', 'incorta'),
						'desc' => esc_html__('Add a icon here.', 'incorta'),
					),
					array(
						'id'    => 'footer_contact_info_text',
						'type'  => 'text',
						'title' => esc_html__('Contact Info Text', 'incorta'),
						'desc' => esc_html__('Type contact info text here.', 'incorta'),
					),
				),
			),

			array(
				'id'        => 'footer_copyright_text',
				'type'      => 'textarea',
				'title'     => esc_html__('Footer Copyright Text.', 'incorta'),
				'desc' => esc_html__('Enter footer text here.', 'incorta'),
				'default'   => esc_html__('Copyright 2018 incorta - All Rights Reserved.', 'incorta'),
			),
		)
	);     

	return $options;
}
add_filter('cs_framework_options', 'incorta_theme_options');